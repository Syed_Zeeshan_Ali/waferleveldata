"""
A module to create a heatmap for calculated values on the wafer.
@author: Ethan
"""

import pandas as pd
import matplotlib.pyplot as plt
from wfmap import wafermap

from FolderInfo import getFolderList
from FullData import IVRawData


def map_pixels_to_reticle(reticle_num):
    """
    This function creates the co-ordinate system for the reticle.
    :param reticle_num:
    :return: pixel_df
    """
    # To note: reticle_num starts at 0

    # Creating the snaking pattern that the probe station measures each reticle device.
    x_inc = list(range(1, 13, 1))
    x_dec = list(range(12, 0, -1))

    x = (x_inc + x_dec) * 7 + x_inc
    y = [n for n in range(1, 16, 1) for i in range(12)]

    pixel_df = pd.DataFrame({'x': x,
                             'y': y})

    # Drop the un-used co-ordinates in the reticle
    pixel_df.drop(index=(pixel_df[(pixel_df['x'] == 1) & (pixel_df['y'] == 1)].index.tolist()), inplace=True)  # 1,1

    pixel_df.drop(index=(pixel_df[(pixel_df['x'] == 1) & (pixel_df['y'] == 10)].index.tolist()), inplace=True)  # 1, 10
    pixel_df.drop(index=(pixel_df[(pixel_df['x'] == 12) & (pixel_df['y'] == 10)].index.tolist()), inplace=True)  # 12,10

    pixel_df.drop(index=(pixel_df[(pixel_df['x'] == 12) & (pixel_df['y'] == 15)].index.tolist()), inplace=True)  # 12,15
    pixel_df.drop(index=(pixel_df[(pixel_df['x'] == 11) & (pixel_df['y'] == 15)].index.tolist()), inplace=True)  # 11,15
    pixel_df.drop(index=(pixel_df[(pixel_df['x'] == 12) & (pixel_df['y'] == 14)].index.tolist()), inplace=True)  # 12,14
    pixel_df.drop(index=(pixel_df[(pixel_df['x'] == 11) & (pixel_df['y'] == 14)].index.tolist()), inplace=True)  # 11,14

    # Add pixel numbers
    pixel_total = len(pixel_df)
    pixel_num = [i + pixel_total * reticle_num for i in range(1, pixel_total + 1, 1)]
    pixel_df['pixel_num'] = pixel_num

    """
    print(f"len(x) = {len(x)}")
    print(f"len(y) = {len(y)}")
    print(f"First pixel = {pixel_num[0]}")
    print(f"First pixel = {pixel_num[-1]}")
    """
    print(f"Total devices on reticle = {len(pixel_num)}")
    plt.scatter(x=pixel_df['x'], y=pixel_df['y'])
    return pixel_df


def map_pixel_to_wafer(x_reticle, y_reticle, x_offset, y_offset):
    """
    This function takes the reticle co-ordinates and creates x and y offsets to
    create co-ordinates for each reticle position
    """
    x = (x_offset) * 12 + x_reticle
    y = (y_offset) * 15 + y_reticle

    return x, y


if __name__ == '__main__':

    df = map_pixels_to_reticle(reticle_num=32)

    # Analyse data
    
    TestTemperatures = [25]
    FolderList = getFolderList(TestTemperatures)
    main_data_set = IVRawData(FolderList, TestTemperatures)
    main_data_set.getSingleDataFrame()
    main_data_set.MainDataSet.sort_values('subdie', ascending=True, inplace=True)
    main_data_set = main_data_set.MainDataSet
    # save data to csv for quicker script running after analysis (comment out line 75 to 83 if looking at same data.
    main_data_set.to_csv("main_data_set.csv", index=False)
    
    #main_data_set = pd.read_csv("main_data_set.csv")

    # Get local wafer
    wafer_local = pd.DataFrame()
    for i in range(32):
        reticle_local = map_pixels_to_reticle(reticle_num=i)
        wafer_local = pd.concat([wafer_local, reticle_local])

    # Populate wafer dataframe
    wafer = pd.DataFrame(columns=['MAP_COL', 'MAP_ROW', 'R25OnMem'])
    for index, pixel in main_data_set.iterrows():
        pixel_num = int(pixel['subdie'])
        x_offset = pixel.LocX
        y_offset = pixel.LocY

        pixel_local = wafer_local[wafer_local['pixel_num'] == pixel_num].reset_index()
        x_reticle = pixel_local.loc[0, 'x']
        y_reticle = pixel_local.loc[0, 'y']

        x, y = map_pixel_to_wafer(x_reticle, y_reticle, x_offset, y_offset)

        wafer.loc[pixel_num, 'MAP_COL'] = x
        wafer.loc[pixel_num, 'MAP_ROW'] = y
        wafer.loc[pixel_num, 'pixel_num'] = pixel_num
        wafer.loc[pixel_num, 'R25OnMem'] = pixel.R25OnMem
        wafer.loc[pixel_num, 'R25OffMem'] = pixel.R25OffMem
        wafer.loc[pixel_num, 'P25'] = pixel.P25

    # Filtering data
    wafer_filtered_R25On = wafer[(wafer['R25OnMem'] < 250) & (wafer['R25OnMem'] > 100)]
    wafer_filtered_R25On = wafer_filtered_R25On.astype(dtype='float', copy=True)

    wafer_filtered_R25Off = wafer[(wafer['R25OffMem'] < 250) & (wafer['R25OffMem'] > 100)]
    wafer_filtered_R25Off = wafer_filtered_R25Off.astype(dtype='float', copy=True)

    # Create heatmaps
    print('Pixel number map')
    wafermap(wafer, value='pixel_num', dtype='num')

    print('R25OnMem map')
    wafermap(wafer_filtered_R25On, value='R25OnMem', dtype='num')

    print('R25OffMem map')
    wafermap(wafer_filtered_R25Off, value='R25OffMem', dtype='num')

    print('P25')
    wafermap(wafer, value='P25', dtype='num')

    plt.show()
    print("Script finished")
