# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 11:22:57 2020

@author: Zeeshan
"""

import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from BasicCalcs import *
from DataPlots import *


def cleanUpDataTCR(MainDataSet):
    
    MainDataSet = MainDataSet[(MainDataSet['RHeater 25C']>=80) & (MainDataSet['RHeater 25C']<=160)]
    MainDataSet = MainDataSet[(MainDataSet['ROffMem 25C']>=80) & (MainDataSet['ROffMem 25C']<=160)]
    MainDataSet = MainDataSet[(MainDataSet['RHeater 75C']>=80) & (MainDataSet['RHeater 75C']<=160)]
    MainDataSet = MainDataSet[(MainDataSet['ROffMem 75C']>=80) & (MainDataSet['ROffMem 75C']<=160)]
    MainDataSet = MainDataSet[(MainDataSet['RHeater 125C']>=80) & (MainDataSet['RHeater 125C']<=160)]
    MainDataSet = MainDataSet[(MainDataSet['ROffMem 125C']>=80) & (MainDataSet['ROffMem 125C']<=160)]
    
    return MainDataSet

def getTweakedTCR1(R0Resistance):
    return 0.0022 - 0.0015*(R0Resistance-118)/340

def TestWithTCR(MainDataSet,Status):
    MainDataSet['CalcR75'] = calculateResistance(MainDataSet['ROffMem 25C'],75,MainDataSet['TCR1'],TCR2)
    MainDataSet['CalcR125'] = calculateResistance(MainDataSet['ROffMem 25C'],125,MainDataSet['TCR1'],TCR2)
    
    MainDataSet['HCalcR75'] = calculateResistance(MainDataSet['RHeater 25C'],75,MainDataSet['TCR1'],TCR2)
    MainDataSet['HCalcR125'] = calculateResistance(MainDataSet['RHeater 25C'],125,MainDataSet['TCR1'],TCR2)
    
    MainDataSet['Error75'] = MainDataSet['CalcR75'] - MainDataSet['ROffMem 75C']
    MainDataSet['Error125'] = MainDataSet['CalcR125'] - MainDataSet['ROffMem 125C']
     
    MainDataSet['HError75'] = MainDataSet['HCalcR75'] - MainDataSet['RHeater 75C']
    MainDataSet['HError125'] = MainDataSet['HCalcR125'] - MainDataSet['RHeater 125C']
    
    MainDataSet = cleanUpDataTCR(MainDataSet)
    
    plotForScatter(MainDataSet,['ROffMem 25C','ROffMem 25C','ROffMem 25C','ROffMem 25C'],['ROffMem 75C','CalcR75','ROffMem 125C','CalcR125'],['Measured R75','Calculated R75','Measured R125','Calculated R125'],'Off Membrane Resistance at 25C (ohms)','High Temperature Off Membrane Resistance' + Status + ' Calculation ' + '(ohms)')
    plotForScatter(MainDataSet,['ROffMem 25C','ROffMem 25C'],['Error75','Error125'],['Error75','Error125'],'Off Membrane Resistance at 25C (ohms)','Error between measured and' + Status +  'calculated (ohms)')
    
    plotForScatter(MainDataSet,['RHeater 25C','RHeater 25C','RHeater 25C','RHeater 25C'],['RHeater 75C','HCalcR75','RHeater 125C','HCalcR125'],['Measured R75','Calculated R75','Measured R125','Calculated R125'],'Heater Resistance at 25C (ohms)','High Temperature Heater Resistance' + Status + ' Calculation ' + '(ohms)')
    plotForScatter(MainDataSet,['RHeater 25C','RHeater 25C'],['HError75','HError125'],['HError75','HError125'],'Heater Resistance at 25C (ohms)','Error between measured and' + Status +  'calculated (ohms)')
    
    PrintStats(MainDataSet,'Error75')
    PrintStats(MainDataSet,'Error125')
    
    

def AnalyseTCR(MainDataSet):
    MainDataSet['TCR1'] = getTCR1(MainDataSet['ROffMem 25C'])
    TestWithTCR(MainDataSet,'Current')
    
    MainDataSet['TCR1'] = getTweakedTCR1(MainDataSet['ROffMem 25C'])
    TestWithTCR(MainDataSet,'Tweaked')