# -*- coding: utf-8 -*-
"""
Created on Tue Feb 25 10:47:59 2020

@author: Zeeshan
"""
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from tkinter import filedialog

def increaseFontSize():
    plt.rcParams.update({'font.size': 24})
    matplotlib.rc('xtick', labelsize=10) 
    matplotlib.rc('ytick', labelsize=16) 

def plotForScatter(MainDataSet,ColumnListX,ColumnListY,LabelList,XLabel,YLabel):
    ColourList = ['Blue','DarkGreen','Red','Orange','Yellow','Purple']
    
    increaseFontSize() 
    
    Plot1 = MainDataSet.plot.scatter(x=ColumnListX[0],y=ColumnListY[0], color=ColourList[0], label = LabelList[0])
    
    for NumX in range(len(ColumnListY)-1):
        MainDataSet.plot.scatter(x=ColumnListX[NumX+1],y=ColumnListY[NumX+1], color=ColourList[NumX+1],ax=Plot1, label = LabelList[NumX+1])
    
    Plot1.set_xlabel(XLabel)
    Plot1.set_ylabel(YLabel)
    
    Plot1.legend()
    
    return Plot1

def plotForScatterExisting(MainDataSet,ColumnListX,ColumnListY,LabelList,XLabel,YLabel,Plot1, Number):
    ColourList = ['Blue','DarkGreen','Red','Orange','Yellow','Purple']
    
    increaseFontSize() 
    
    MainDataSet.plot.scatter(x=ColumnListX[0],y=ColumnListY[0], color=ColourList[0+Number], label = LabelList[0],ax=Plot1)
    
    for NumX in range(len(ColumnListY)-1):
        MainDataSet.plot.scatter(x=ColumnListX[NumX+1],y=ColumnListY[NumX+1], color=ColourList[NumX+1],ax=Plot1, label = LabelList[NumX+1])
    
    Plot1.set_xlabel(XLabel)
    Plot1.set_ylabel(YLabel)
    
    Plot1.legend()
    
    return Plot1
    
    
def plotXY(MainDataSet,ColumnX,ColumnListY,LabelList,XLabel,YLabel):
    
    increaseFontSize()
    
    Plot1 = MainDataSet.plot(x=ColumnX,y=ColumnListY,label = LabelList)
    Plot1.set_xlabel(XLabel)
    Plot1.set_ylabel(YLabel)
    
    Plot1.legend()
    

def PlotLine(MainDataSet,ColumnListY,LabelList,XLabel,YLabel):
    
    increaseFontSize()
    
    Plot1 = MainDataSet.plot(y=ColumnListY,label = LabelList)
    Plot1.set_xlabel(XLabel)
    Plot1.set_ylabel(YLabel)
    
    Plot1.legend()
    
    
def plotHist(MainDataSet,ColumnY,LabelList,BinSize):
    
    increaseFontSize() 
    plt.figure()
    
    PlotSet = MainDataSet[ColumnY]
    
    Plot1 = PlotSet.plot.hist(bins = BinSize, label = LabelList)
    Plot1.set_xlabel(LabelList)
    
    
def scatterPlotDataframe2D(DataFrameArray,ColumnX,ColumnY,XLabel,YLabel):
    increaseFontSize()
    
    Plot1 = DataFrameArray[0][0].plot.scatter(x=ColumnX,y=ColumnY)
    
    for Single1DArray in DataFrameArray:
        for SingleDataFrame in Single1DArray:
            SingleDataFrame.plot.scatter(x=ColumnX,y=ColumnY,ax=Plot1)
    
    Plot1.set_xlabel(XLabel)
    Plot1.set_ylabel(YLabel)
    
def scatterPlotDataframe1D(DataFrameArray,ColumnX,ColumnY,XLabel,YLabel):
    increaseFontSize()
    
    Plot1 = DataFrameArray[0].plot.scatter(x=ColumnX,y=ColumnY)
    
    for SingleDataFrame in DataFrameArray:
        SingleDataFrame.plot.scatter(x=ColumnX,y=ColumnY,ax=Plot1)
    
    Plot1.set_xlabel(XLabel)
    Plot1.set_ylabel(YLabel)
    

def scatterPlotDataframe1D_Remove(DataFrameArray,ColumnX,ColumnY,XLabel,YLabel,R25):
    increaseFontSize()
    
    Fig1 = matplotlib.pyplot.figure(figsize=(20.0, 10))
    AX1 = Fig1.add_subplot(111)
    
    
    i=0
    
    for SingleDataFrame in DataFrameArray:
        if R25[i] > 500:
            i=i+1
            continue
        if R25[i] < -500:
            i=i+1
            continue
        print(i)
        SingleDataFrame.plot.scatter(x=ColumnX,y=ColumnY,ax=AX1)
        i=i+1
    
    AX1.set_xlabel(XLabel)
    AX1.set_ylabel(YLabel)
    
    
    
    
def PlotDataframe1D(DataFrameArray,ColumnX,ColumnY,XLabel,YLabel):
    increaseFontSize()
    
    Plot1 = DataFrameArray[0].plot(x=ColumnX,y=ColumnY)
    
    for SingleDataFrame in DataFrameArray:
        SingleDataFrame.plot(x=ColumnX,y=ColumnY,ax=Plot1)
    
    Plot1.set_xlabel(XLabel)
    Plot1.set_ylabel(YLabel)
    
    Plot1.legend([])
    
    return Plot1


def PlotDataframe1D_Remove(DataFrameArray,ColumnX,ColumnY,XLabel,YLabel,R25):
    increaseFontSize()
    
    Plot1 = DataFrameArray[0].plot(x=ColumnX,y=ColumnY)
    
    i=0
    
    for SingleDataFrame in DataFrameArray:
        if (R25[i] >-500) & (R[i]<500): 
            SingleDataFrame.plot(x=ColumnX,y=ColumnY,ax=Plot1)
        i=i+1
    
    Plot1.set_xlabel(XLabel)
    Plot1.set_ylabel(YLabel)
    
    Plot1.legend([])
    
    return Plot1

def PlotDataframe1DExisting(DataFrameArray,ColumnX,ColumnY,XLabel,YLabel,Plot1):
    increaseFontSize()
    
    for SingleDataFrame in DataFrameArray:
        SingleDataFrame.plot(x=ColumnX,y=ColumnY,ax=Plot1)
    
    Plot1.set_xlabel(XLabel)
    Plot1.set_ylabel(YLabel)
    
    Plot1.legend([])
    
    return Plot1

def getSingleDataFrameRow(DataFrameArray2D,DataFrameRow,ColumnY):
    SingleArray = []
    
    for DataFrame1DArray in DataFrameArray2D:
        for SingleFrame in DataFrame1DArray:
            SingleArray.append(SingleFrame[ColumnY][DataFrameRow])
    

   
        
def plotHistogram2DArray(DataFrameArray,DataFrameRow,ColumnY,YLabel):
    increaseFontSize()
    
    
    
    Plot1 = DataFrameArray[0][0].plot.scatter(x=ColumnX,y=ColumnY)
    
    for Single1DArray in DataFrameArray:
        for SingleDataFrame in Single1DArray:
            SingleDataFrame.plot.scatter(x=ColumnX,y=ColumnY,ax=Plot1)
    
    