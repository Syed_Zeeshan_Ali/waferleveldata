# -*- coding: utf-8 -*-
"""
Created on Tue Aug 18 14:44:46 2020

@author: Zeeshan
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import Globals
from Globals import *
from BasicCalcs import *
from ReadFile import *
from DataPlots import *
from ResistanceCompare import *
from TCRAnalysis import *
from R25Determination import *
from PowerAnalysis import *
from tkinter import filedialog
from scipy.optimize import curve_fit
from FullData import *
from FolderInfo import *


#legacy, leave this as is
ChipNumbers = [1]

#Gets the folders needed. assumes one folder for each test temperature
#"TestTemperature" list should constain each test temperature. It will then ask to choose that many folders. 
#e.g. TestTemperature=[25,75,125] will ask for locations of 3 folders, and will assume measurement temperature are 25C, 75C and 125C
TestTemperatures = [25,75,125]
FolderList = getFolderList(TestTemperatures)

#Generates data from the folders and stores in CompleteData 
# Following assumptions are made about the files:
#   All files start with either "Off" or "On" for if they are off or on membrane measurements
#   All files are .csv (will ignore any other files in the folder)
#   All files end with  X Y _Z  , where X and Y give the coordinate of the reticle, and Z is a sequence number.
CompleteData = IVRawData(FolderList,TestTemperatures)
CompleteData.getSingleDataFrame()

# plot Resistances
for singleTemp in TestTemperatures:
    PlotLine(CompleteData.MainDataSet,'R' + str(singleTemp) + 'OffMem','Off Membrane'+str(singleTemp)+'C','','Resistance (ohms)')
    PlotLine(CompleteData.MainDataSet,'R' + str(singleTemp) + 'OnMem','On Membrane'+str(singleTemp)+'C','','Resistance (ohms)')



################################################
# Special Test for resistance change

difference = []

for i in range(len(CompleteData.MainDataSet)):
    
    if i == 0:
        continue
    
    if (CompleteData.MainDataSet['LocX'][i] == CompleteData.MainDataSet['LocX'][i-1]) and (CompleteData.MainDataSet['LocY'][i] == CompleteData.MainDataSet['LocY'][i-1]):
        
        if (CompleteData.MainDataSet['R25OffMem'][i] != np.nan) and (CompleteData.MainDataSet['R25OffMem'][i-1] != np.nan):
                DiffNum = CompleteData.MainDataSet['R25OffMem'][i]-CompleteData.MainDataSet['R25OffMem'][i-1]
                
                if (np.isnan(DiffNum) == False) and (DiffNum > -10) and (DiffNum < 10):
                    difference.append(DiffNum)
        
    if i%200 == 0:
        print(i)


print(np.min(difference))
print(np.max(difference))
print(np.std(difference))


for i in range(len(difference)):
    
    if np.isnan(difference[i]):
        print('nan!')