import pandas as pd
import numpy as np
import glob
from Globals import *
from BasicCalcs import *

##############################################################################################################################
# For Reading Single Files

def getFileData(FilePath):
    StartNum = getStartingLineNumberForFile(FilePath,"Test data")
    MainDataSet = pd.read_csv(FilePath,',')
    return MainDataSet

def getStartingLineNumberForFile(FileName,lookup):
    with open(FileName) as myFile:
        for num, line in enumerate(myFile, 1):
            if lookup in line:
                return num
        myFile.close();
    return 0

################################################################################################################################
# For Reading in the main processed Wafer Level test data files 


def ReturnData(FilePath,Temp):
    FullDataSet = getFileData(FilePath)

    OffMemDataSet = FullDataSet[FullDataSet['Subdev']==1]
    OnMemDataSet = FullDataSet[FullDataSet['Subdev']==0]
    
    ColumnName = str(Temp) + "C lin offset"
    MainDataSet = OffMemDataSet[['Device#','Subdie',ColumnName]]
    MainDataSet.columns = ['Device','Subdie','ROffMem ' + str(Temp) + 'C' ]
    
    ColumnName = str(Temp) + "C quad offset"
    MainDataSet['RHeater ' + str(Temp) + 'C'] = OnMemDataSet[ColumnName].to_numpy()
    
    return MainDataSet

def AddData(FilePath,Temp,MainDataSet):
    FullDataSet = getFileData(FilePath)

    OffMemDataSet = FullDataSet[FullDataSet['Subdev']==1]
    OnMemDataSet = FullDataSet[FullDataSet['Subdev']==0]
    
    ColumnName = str(Temp) + "C lin offset"
    MainDataSet['ROffMem ' + str(Temp) + 'C'] = OffMemDataSet[ColumnName]
    
    ColumnName = str(Temp) + "C quad offset"
    MainDataSet['RHeater ' + str(Temp) + 'C'] = OnMemDataSet[ColumnName].to_numpy()
    
    return MainDataSet                        

#############################################################################################################################
# For Reading in the raw wafer level test files

#gets a dataframe with the current and voltage values from the file
def getDataFromFile(FilePathAndName):
    StartNum = getStartingLineNumberForFile(FilePathAndName,"DataName")
    FileData=pd.read_csv(FilePathAndName,skiprows=StartNum, usecols=[1,2])
    FileData.columns = ['Current','Voltage'] 
    
    return FileData

def IsItOnMembrane(FilePathAndName):
    if "On membrane" in FilePathAndName:
            return True
    else:
            return False

def getDeviceLocation(FilePathAndName):
    FileNameSplit = FilePathAndName.split("_")
    
    SplitLen = len(FileNameSplit)
    
    
    ReticleX = int(FileNameSplit[SplitLen-2].split(" ")[1])
    ReticleY = int(FileNameSplit[SplitLen-2].split(" ")[2])
    DevNo = int(FileNameSplit[SplitLen-1].split(".")[0])
    
    return (ReticleX,ReticleY,DevNo)

def getDeviceIndex(FilePathAndName,DeviceList):
    FileNameSplit = FilePathAndName.split("_")
    SplitLen = len(FileNameSplit)
    
    DeviceIndex = int(FileNameSplit[SplitLen-1].split(".")[0])%len(DeviceList)  #Reads file sequence number and mods with no of devices.
    
    return DeviceIndex-1 #To set the first device from 0 rather than 1

def CalculateFileData(SingleDataSet):
    SingleDataSet['Resistance'] = SingleDataSet['Voltage']/SingleDataSet['Current']
    SingleDataSet['Power'] = SingleDataSet['Voltage']*SingleDataSet['Current']
    R25 = SingleDataSet['Resistance'][0]
    TCR1 = getTCR1(R25)
    SingleDataSet['Temperature'] = getTemperatureFromResistance(SingleDataSet['Resistance'], R25, TCR1,TCR2,25)
    return SingleDataSet
    
#Main Function to call to get file data
def CollectDataFromFolder(FolderPath,DeviceList):
    
    #Create Empty Data Lists
    OnMembraneIVData = []
    OffMembraneIVData = []
    OnMembraneLocationData = []
    OffMembraneLocationData = []
    count=0
    
    for i in DeviceList:
        OnMembraneIVData.append([])
        OffMembraneIVData.append([])
        OnMembraneLocationData.append([])
        OffMembraneLocationData.append([])
    
    #Get list of all .csv files in folder
    FileList = sorted(glob.glob(FolderPath + "\\*.csv"))   
    
    for SingleFileName in FileList:
        try:
            SingleFileDataRaw  = getDataFromFile(SingleFileName)
        except:
            continue
        SingleFileData  = CalculateFileData(SingleFileDataRaw)
        OnMembrane      = IsItOnMembrane(SingleFileName)
        ReticleLocation = getDeviceLocation(SingleFileName)
        DeviceIndex     = getDeviceIndex(SingleFileName,DeviceList)
        
        if OnMembrane == True:
            OnMembraneIVData[DeviceIndex].append(SingleFileData)
            OnMembraneLocationData[DeviceIndex].append(ReticleLocation)
        else:
            OffMembraneIVData[DeviceIndex].append(SingleFileData)
            OffMembraneLocationData[DeviceIndex].append(ReticleLocation)
    
        if(count%100==0):
            print(count)
        #print(SingleFileName)
        count = count + 1
    
    return OnMembraneIVData,OffMembraneIVData,OnMembraneLocationData,OffMembraneLocationData
    
    