import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import Globals
from Globals import *
from BasicCalcs import *
from ReadFile import *
from DataPlots import *
from ResistanceCompare import *
from TCRAnalysis import *
#from ExtraPlots import *
#from PiccoloPlot import *
#from CurveFitsAndPlots import *
from tkinter import filedialog
from scipy.optimize import curve_fit





#Get path and names of Electrical and flow files
FilePath1 = filedialog.askopenfilename(title='Open 25C LogFile')
FilePath2 = filedialog.askopenfilename(title='Open 75C LogFile')
FilePath3 = filedialog.askopenfilename(title='Open 125C LogFile')

MainDataSet = ReturnData(FilePath1,25)
MainDataSet = AddData(FilePath2,75,MainDataSet)
MainDataSet = AddData(FilePath3,125,MainDataSet)


#MainDataSet = MainDataSet[MainDataSet['Subdie']==3]


#ResistanceComparison(MainDataSet)
#AnalyseTCR(MainDataSet)

plotHist(MainDataSet,'ROffMem 25C','Temp Sensor Resistance at 25C (ohms)',100)
plotHist(MainDataSet,'ROffMem 75C','Temp Sensor Resistance at 75C (ohms)',100)
plotHist(MainDataSet,'ROffMem 125C','Temp Sensor Resistance at 125C (ohms)',100)


MainDataSet['Col25C'] = pd.Series([25 for x in range(len(MainDataSet))])
MainDataSet['Col75C'] = pd.Series([75 for x in range(len(MainDataSet))])
MainDataSet['Col125C'] = pd.Series([125 for x in range(len(MainDataSet))])

Plot1 = plotForScatter(MainDataSet,['Col25C','Col75C','Col125C'],['ROffMem 25C','ROffMem 75C','ROffMem 125C'],['25C','75C','125C'],'Temperature (degC)','Resistance (ohms)')





