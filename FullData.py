# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 15:04:24 2020

@author: Zeeshan
"""

import pandas as pd
from BasicCalcs import *
from IVSingleRawData import *
import glob


#Stores raw IV data

class IVRawData:
    FolderList = []
    TempList = []    
    OnMemData = []
    OffMemData = []
    MainDataSet = pd.DataFrame()
    
    def populateData(self,inputFolderList,inputTempList):
        
        TempIndex = 0
        
        for singleFolder in inputFolderList:
            FileList = sorted(glob.glob(singleFolder + "\\*.csv"))            # gets list of all .csv files in folder
            
            TempOnMemIV = []
            TempOffMemIV = []
            
            i=0
            
            for SingleFileName in FileList:
                
                OnMembrane      = self.IsItOnMembrane(SingleFileName)
                
                try:
                    SingleFileData = IVSingleRawData(SingleFileName,inputTempList[TempIndex],OnMembrane)
                except:
                    continue
                
                #if SingleFileData.checkResistance() == False:
                #    continue
        
                if OnMembrane == True:
                    TempOnMemIV.append(SingleFileData)
                else:
                    TempOffMemIV.append(SingleFileData)
                
                if i%200 == 0:
                    print (i)
            
                i=i+1
            
            self.OnMemData.append(TempOnMemIV)
            self.OffMemData.append(TempOffMemIV)
            
            TempIndex = TempIndex + 1
            
        
    
    def __init__(self,inputFolderList,inputTempList):
        self.FolderList = inputFolderList
        self.TempList = inputTempList
        self.populateData(self.FolderList,self.TempList)
    

    def IsItOnMembrane(self,FilePathAndName):
        if "On membrane" in FilePathAndName:
                return True
        else:
                return False

    
    


##########################################################################################
# SingleDataFrame5
                '''
    def getSingleDataFrame(self):
        dfColumns = ['LocX','LocY','subdie','R25OffMem','R25OnMem','R75OffMem','R75OnMem','R125OffMem','R125OnMem','P25','P75','P125']
        
        MainData = pd.DataFrame(columns = dfColumns)
        
        for SingleSet in self.OffMemData[0]:
            X = SingleSet.LocationX
            Y = SingleSet.LocationY
            subdie = SingleSet.ItemNo
            R25Off = SingleSet.ResistanceT25
            
            R25On, Power25 = self.getRTandPower230(self.OnMemData[0],X,Y,subdie)
            R75Off,Power75 = self.getRTandPower230(self.OffMemData[1],X,Y,subdie)
            R75On,Power75 = self.getRTandPower230(self.OnMemData[1],X,Y,subdie)
            R125Off,Power125 = self.getRTandPower230(self.OffMemData[2],X,Y,subdie)
            R125On,Power125 = self.getRTandPower230(self.OnMemData[2],X,Y,subdie)
        
            MainData = MainData.append({'LocX':X,'LocY':Y,'subdie':subdie,'R25OffMem':R25Off,'R25OnMem':R25On,
                                        'R75OffMem':R75Off,'R75OnMem':R75On,'R125OffMem':R125Off,'R125OnMem':R125On,
                                        'P25':Power25,'P75':Power75,'P125':Power125},ignore_index=True)
    
        for SingleSet in self.OffMemData[1]:
            X = SingleSet.LocationX
            Y = SingleSet.LocationY
            subdie = SingleSet.ItemNo
            R25Off = np.nan
            R25On = np.nan
            R75Off = SingleSet.ResistanceT25
            Power25 = np.nan
            
            R75On,Power75 = self.getRTandPower230(self.OnMemData[1],X,Y,subdie)
            R125Off,Power125 = self.getRTandPower230(self.OffMemData[2],X,Y,subdie)
            R125On,Power125 = self.getRTandPower230(self.OnMemData[2],X,Y,subdie)
            
            MainData = MainData.append({'LocX':X,'LocY':Y,'subdie':subdie,'R25OffMem':R25Off,'R25OnMem':R25On,
                                        'R75OffMem':R75Off,'R75OnMem':R75On,'R125OffMem':R125Off,'R125OnMem':R125On,
                                        'P25':Power25,'P75':Power75,'P125':Power125},ignore_index=True)
    
        for SingleSet in self.OffMemData[2]:
            X = SingleSet.LocationX
            Y = SingleSet.LocationY
            subdie = SingleSet.ItemNo
            R25Off = np.nan
            R25On = np.nan
            R75Off = np.nan
            R75On = np.nan
            R125Off = SingleSet.ResistanceT25
            Power25 = np.nan
            Power75 = np.nan
            
            R125On,Power125 = self.getRTandPower230(self.OnMemData[2],X,Y,subdie)
            
            MainData = MainData.append({'LocX':X,'LocY':Y,'subdie':subdie,'R25OffMem':R25Off,'R25OnMem':R25On,
                                        'R75OffMem':R75Off,'R75OnMem':R75On,'R125OffMem':R125Off,'R125OnMem':R125On,
                                        'P25':Power25,'P75':Power75,'P125':Power125},ignore_index=True)
    
        return MainData   '''
    
    def getTempStrings(self,singleTemp):
        a = 'R' + str(singleTemp) + 'OffMem'
        b = 'R' + str(singleTemp) + 'OnMem'
        c = 'P' + str(singleTemp)
        return [a,b,c]
    
    def generateColumnList(self):
        dfColumns = ['LocX','LocY','subdie']
        
        for singleTemp in self.TempList:
            dfColumns = dfColumns + self.getTempStrings(singleTemp)
        
        return dfColumns
    
    
    def getSingleDataFrame(self):
        
        dfColumns = self.generateColumnList()
        
        self.MainDataSet = pd.DataFrame(columns = dfColumns)
        
        i = 0
        
        for singleTemp in self.TempList:
            
            
            
            for SingleSet in self.OffMemData[i]:
                X = SingleSet.LocationX
                Y = SingleSet.LocationY
                subdie = SingleSet.ItemNo
                
                if self.checkLocationAlreadyExisting(self.MainDataSet,X,Y,subdie) == True:
                    continue
                
                SingleRow = {'LocX':X,'LocY':Y,'subdie':subdie}
                
                j = 0
                
                for singleSubTemp in self.TempList:
                                
                    TempColumnStrings = self.getTempStrings(singleSubTemp)
                    
                    #if singleTemp == singleSubTemp:
                    RTOff,Power0 = self.getRTandPower230(self.OffMemData[j],X,Y,subdie)
                    RTOn,PowerT = self.getRTandPower230(self.OnMemData[j],X,Y,subdie)
                    #else:
                        #RTOff = RTOn = PowerT = np.nan
                
                    SingleRow[TempColumnStrings[0]] = RTOff
                    SingleRow[TempColumnStrings[1]] = RTOn
                    SingleRow[TempColumnStrings[2]] = PowerT
                    
                    j = j+1
                
                self.MainDataSet = self.MainDataSet.append(SingleRow,ignore_index=True)
            
            i = i+1
                    
    
    
    def checkLocationAlreadyExisting(self,inputDataFrame,X,Y,subdie):
        newDataFrame = inputDataFrame[(inputDataFrame['LocX']==X)&(inputDataFrame['LocY']==Y)&(inputDataFrame['subdie']==subdie)]
        
        if len(newDataFrame)==0:
            return False
        
        return True
    
        
    def getRTandPower230(self,RawDataList,XLoc,YLoc,Item):
        
        RT = np.nan
        Power = np.nan
        
        for SingleData in RawDataList:
            if (SingleData.LocationX == XLoc) & (SingleData.LocationY == YLoc) & (SingleData.ItemNo == Item):
                RT = SingleData.ResistanceTMeas
                Power = np.nan
                if SingleData.OnMembrane == True:
                    Power = SingleData.Power230
        
        return RT,Power