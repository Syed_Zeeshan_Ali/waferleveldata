# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 09:44:50 2020

@author: Zeeshan
"""

import numpy as np
import pandas as pd
from ReadFile import *
import matplotlib.pyplot as plt
import matplotlib
from DataPlots import *
from BasicCalcs import *


def cleanUpData(MainDataSet):
    
    MainDataSet = MainDataSet[(MainDataSet['RHeater 25C']>=80) & (MainDataSet['RHeater 25C']<=160)]
    MainDataSet = MainDataSet[(MainDataSet['ROffMem 25C']>=80) & (MainDataSet['ROffMem 25C']<=160)]
    MainDataSet = MainDataSet[(MainDataSet['RHeater 75C']>=80) & (MainDataSet['RHeater 75C']<=160)]
    MainDataSet = MainDataSet[(MainDataSet['ROffMem 75C']>=80) & (MainDataSet['ROffMem 75C']<=160)]
    MainDataSet = MainDataSet[(MainDataSet['RHeater 125C']>=80) & (MainDataSet['RHeater 125C']<=160)]
    MainDataSet = MainDataSet[(MainDataSet['ROffMem 125C']>=80) & (MainDataSet['ROffMem 125C']<=160)]
    MainDataSet = MainDataSet[(MainDataSet['Ratio 25C']>=0.9) & (MainDataSet['Ratio 25C']<=1)]
    MainDataSet = MainDataSet[(MainDataSet['Ratio 75C']>=0.9) & (MainDataSet['Ratio 75C']<=1)]
    MainDataSet = MainDataSet[(MainDataSet['Ratio 125C']>=0.9) & (MainDataSet['Ratio 125C']<=1)]
    
    return MainDataSet


def getRatio(MainDataSet,Temp):
    MainDataSet['Ratio ' + str(Temp) +'C'] = MainDataSet['RHeater ' + str(Temp) + 'C'] /  MainDataSet['ROffMem ' + str(Temp) + 'C']
    return MainDataSet

def PlotPerDevice(MainDataSet):
    
    Dev1 = MainDataSet[MainDataSet['Subdie']==1]
    Dev2 = MainDataSet[MainDataSet['Subdie']==2]
    Dev3 = MainDataSet[MainDataSet['Subdie']==3]
    
    Plot1 = plotForScatter(Dev1,['ROffMem 25C'],['RHeater 25C'],['Heater Resistance Chip01'],'2-Wire Temperature Sensor Resistance','4-Wire Heater Resistance')
    plotForScatterExisting(Dev2,['ROffMem 25C'],['RHeater 25C'],['Heater Resistance Chip02'],'2-Wire Temperature Sensor Resistance','4-Wire Heater Resistance',Plot1,1)
    plotForScatterExisting(Dev3,['ROffMem 25C'],['RHeater 25C'],['Heater Resistance Chip03'],'2-Wire Temperature Sensor Resistance','4-Wire Heater Resistance',Plot1,2)
    
    PrintStats(Dev1,'Ratio 25C')


def ResistanceComparison(MainDataSet):
    
    MainDataSet = getRatio(MainDataSet,25)
    MainDataSet = getRatio(MainDataSet,75)
    MainDataSet = getRatio(MainDataSet,125)
    
    MainDataSet = cleanUpData(MainDataSet)
    
    PlotPerDevice(MainDataSet)
        
    plotForScatter(MainDataSet,['ROffMem 25C'],['RHeater 25C'],['Heater Resistance'],'2-Wire Temperature Sensor Resistance','4-Wire Heater Resistance')
    plotHist(MainDataSet,['Ratio 25C'],['Ratio 25C'],100)    

    plotForScatter(MainDataSet,['ROffMem 25C','ROffMem 75C','ROffMem 125C'],['RHeater 25C','RHeater 75C','RHeater 125C'],['25C','75C','125C'],'2-Wire Temperature Sensor Resistance','4-Wire Heater Resistance')
    plotHist(MainDataSet,['Ratio 25C','Ratio 75C','Ratio 125C'],['Ratio 25C','Ratio 75C','Ratio 125C'],200)
    
    PrintStats(MainDataSet,'Ratio 25C')
    PrintStats(MainDataSet,'Ratio 75C')
    PrintStats(MainDataSet,'Ratio 125C')
    

########################################################################################################################
#  Compare for Raw Data
    
def ResistanceComparisonRaw(OnMembraneIVData,OffMembraneIVData):
    DataSet = pd.DataFrame([],columns=['RTempSensor','RHeater'])
    
    for i in range(len(OnMembraneIVData)):
        DataSet = DataSet.append({'RTempSensor':OffMembraneIVData[i]['Resistance'][0],'RHeater':OnMembraneIVData[i]['Resistance'][0]},ignore_index=True)
        
    
    return DataSet