# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 16:36:52 2020

@author: Zeeshan
"""

import numpy as np
import pandas as pd
import Globals
from Globals import *
from BasicCalcs import *
import matplotlib.pyplot as plt
import matplotlib
from R25Determination import *

def cleanUpData(SingleDeviceDataFrame):
    
    FrameLength = len(SingleDeviceDataFrame)
    
    FrameIsGood = True
    
    for i in range(FrameLength):
        if (SingleDeviceDataFrame['Current'][i] >0.05 ) or (SingleDeviceDataFrame['Current'][i] < -0.01 ):
            FrameIsGood = False
        
        if (SingleDeviceDataFrame['Temperature'][i] >500 ) or (SingleDeviceDataFrame['Temperature'][i] < 0 ):
            FrameIsGood = False
    
    if FrameIsGood == False:
        SingleDeviceDataFrame['Temperature'] = pd.Series([0 for x in range(FrameLength)])
        SingleDeviceDataFrame['Power'] = pd.Series([0 for x in range(FrameLength)])
    
    return SingleDeviceDataFrame


def populateHeaterTemp(OnMembraneIVData,R25s):
    
    i=0
    
    for SingleDesign in OnMembraneIVData:
        j=0
        for SingleDevice in SingleDesign:
            TCR1 = getTCR1(R25s[i][j])
            SingleDevice['Temperature'] = getTemperatureFromResistance(SingleDevice['Resistance'], 0.967*R25s[i][j], TCR1,TCR2,25)
            SingleDevice['Power'] = SingleDevice['Power'] * 1000

            try:            
                QuadFit = np.polyfit(SingleDevice['Temperature'].to_numpy()[3:], SingleDevice['Power'].to_numpy()[3:], 2)
            except:
                SingleDevice['Temperature'] = pd.Series([0 for x in range(len(SingleDevice))])
                SingleDevice['Power'] = pd.Series([0 for x in range(len(SingleDevice))])
            
            SingleDevice = cleanUpData(SingleDevice)
            
#            PowerAt230C = QuadFit[0]*230*230 + QuadFit[1]*230 + QuadFit[2]
#            PowerCoef = PowerAt230C/13.8
            
#            SingleDevice['AdjustedPower'] = SingleDevice['Power']/PowerCoef
            
            j = j+1
        i = i+1
    
    return OnMembraneIVData


def HeaterR25(OnMembraneIVData,R25s):
    
    NoOfDevices = len(OnMembraneIVData)
    
    OnMembranePowerData = []
    PowerCoefficients = []
    HeaterR25s = []
    
    for i in range(NoOfDevices):
        OnMembranePowerData.append([])
        HeaterR25s.append([])
    
    count=0
    
    
    i=0
    
    for SingleDesign in OnMembraneIVData:
        j=0
        for SingleDevice in SingleDesign:
            
            try:
                QuadFit = np.polyfit(SingleDevice['Power'].to_numpy()[3:], SingleDevice['Resistance'].to_numpy()[3:], 2)
                HeaterR25s[i].append(QuadFit[2])
            except:
                QuadFit = [100,100,1000]
                HeaterR25s[i].append(QuadFit[2])
            
            j = j+1
        i = i+1
    
    
    return HeaterR25s

def populateHeaterTempHeaterR25(OnMembraneIVData,R25s):
    
    NoOfDevices = len(OnMembraneIVData)
    
    OnMembranePowerData = []
    PowerCoefficients = []
    
    for i in range(NoOfDevices):
        OnMembranePowerData.append([])
    
    count=0
    
    
    i=0
    
    for SingleDesign in OnMembraneIVData:
        j=0
        for SingleDevice in SingleDesign:
            #R25 = SingleDevice['Resistance'][0]
            
            print(j)
            
            try:
                QuadFit = np.polyfit(SingleDevice['Power'].to_numpy()[3:], SingleDevice['Resistance'].to_numpy()[3:], 2)
            except:
                QuadFit = [100,100,1000]
            
            TCR1 = getTCR1(QuadFit[2])
            
            SingleDevice['Temperature'] = getTemperatureFromResistance(SingleDevice['Resistance'], QuadFit[2], TCR1,TCR2,25)
            #SingleDevice['Power'] = SingleDevice['Power'] * 1000
            
            QuadFit = np.polyfit(SingleDevice['Temperature'].to_numpy()[3:], SingleDevice['Power'].to_numpy()[3:], 2)
            
            PowerAt100C = QuadFit[0]*100*100 + QuadFit[1]*100 + QuadFit[2]
            PowerAt230C = QuadFit[0]*230*230 + QuadFit[1]*230 + QuadFit[2]
            PowerCoef = PowerAt100C/4.68
            
            SingleDevice['AdjustedPower'] = SingleDevice['Power']/PowerCoef
            
            if(i==2):
                PowerCoefficients.append(PowerCoef)
            
            OnMembranePowerData[i].append(PowerAt230C)
            
            j = j+1
        i = i+1
    
    print("Mean: " + str(np.mean(PowerCoefficients)) + " , STD: " + str(np.std(PowerCoefficients)))
    
    return OnMembraneIVData, OnMembranePowerData


def PlotPowerWaferMap(OnMembranePowerData,OnMembraneLocationData):
    
    PowerData = np.zeros((14,13))
    
    i=0
    
    for SingleLocation in OnMembraneLocationData:
        x,y = SingleLocation
        
        x=13-(x+4)
        y=y
        
        PowerData[x][y]=OnMembranePowerData[i]
        
        i=i+1
    
    
    plt.imshow(PowerData,vmin=13.5,vmax=14)
    plt.show()
    
    return PowerData


def AnalysePower(OffMembraneIVData25,OnMembraneIVData25,OffMembraneIVData75,OnMembraneIVData75,OffMembraneIVData125,OnMembraneIVData125,OnMembraneLocationData):
    #Accurate determination of Power Requirements
    #1. Calculate R25 for all Chip designs
    R25s = calculateR25(OffMembraneIVData25)
    
    #2. For each device and data point calculate heater temperature
    OnMembraneIVData25 = populateHeaterTemp(OnMembraneIVData25,R25s)
    #OnMembraneIVData75 = populateHeaterTemp(OnMembraneIVData75,R25s)
    #OnMembraneIVData125 = populateHeaterTemp(OnMembraneIVData125,R25s)
    
    
    #3. Plot power for each chip 3 device vs heater temperature
    scatterPlotDataframe1D(OnMembraneIVData25[0],'Temperature','Power','Temperature (degC)','Power (mW)')
    #PlotDataframe1D(OnMembraneIVData25[2],'Temperature','Power','Temperature (degC)','Power (mW)')
#    PlotDataframe1D(OnMembraneIVData25[2],'Temperature','AdjustedPower','Temperature (degC)','Adjusted Power (mW)')
    
    #OnMembraneIVData25, OnMembranePowerData = populateHeaterTempHeaterR25(OnMembraneIVData25,R25s)
    #PlotDataframe1D(OnMembraneIVData25[2],'Temperature','Power','Temperature (degC)','Power (mW)')
    #PlotDataframe1D(OnMembraneIVData25[2],'Temperature','AdjustedPower','Temperature (degC)','Adjusted Power (mW)')
    PlotPowerWaferMap(OnMembranePowerData[0],OnMembraneLocationData[0])
    
    #4. Plot power for each chip 3 device vs heater temperature at different ambient temperatures
    #PlotAll = PlotDataframe1D(OnMembraneIVData25[2],'Temperature','Power','Temperature (degC)','Power (mW)')
    #PlotDataframe1DExisting(OnMembraneIVData75[2],'Temperature','Power','Temperature (degC)','Power (mW)',PlotAll)
   # PlotDataframe1DExisting(OnMembraneIVData125[2],'Temperature','Power','Temperature (degC)','Power (mW)',PlotAll)
    
    #5. Extract Power coefficients for #3. Calculate power at 200C and plot Coefficients against P200
