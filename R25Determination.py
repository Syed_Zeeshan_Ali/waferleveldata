# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 11:57:38 2020

@author: Zeeshan
"""

#OffMembraneIVData is a 2D array of dataframes: Current, Voltage, Resistance, Power,Temperature (determined from inital R)
#OffMembraneLocationData is a 2D array of (X,Y) tuples. 

import numpy as np
import pandas as pd
from DataPlots import *
from BasicCalcs import *

def get2DArrayToDataFrame(Array2D,ColumnList):
    
    DataFrameToReturn = pd.DataFrame(columns=ColumnList)
    
    for Array1D in Array2D:
        DataFrameToReturn = DataFrameToReturn.append(pd.DataFrame(Array1D,columns=ColumnList))
    
    return DataFrameToReturn

def getLinearFit(OffMembraneIVData,NoOfInitialPointsToIgnore):
    
    TotalLinearFit = []    
    
    for SingleDesign in OffMembraneIVData:
        
        SingleDesignLinearFit = []
        
        for SingleDevice in SingleDesign:
            SingleLinearFit = np.polyfit(SingleDevice['Power'].to_numpy()[NoOfInitialPointsToIgnore:], SingleDevice['Resistance'].to_numpy()[NoOfInitialPointsToIgnore:], 1)
            SingleDesignLinearFit.append(SingleLinearFit)
            
        TotalLinearFit.append(SingleDesignLinearFit)
    
    return TotalLinearFit


def determineFitError(OffMembraneIVData,LinearFitOffMembrane,NewResistanceColumn,ErrorColumn):
    
    i=0
    
    for SingleDesign in OffMembraneIVData:
        j=0
        for SingleDevice in SingleDesign:
            
            SingleDevice[NewResistanceColumn] = LinearFitOffMembrane[i][j][0]*SingleDevice['Power'] + LinearFitOffMembrane[i][j][1]
            SingleDevice[ErrorColumn] = SingleDevice[NewResistanceColumn] - SingleDevice['Resistance']
                        
            j=j+1
        i=i+1

    return OffMembraneIVData

def CalculateR25FromMeasurement(OffMembraneIVData,LinearFitOffMembrane):
    i=0
    
    for SingleDesign in OffMembraneIVData:
        j=0 
        for SingleDevice in SingleDesign:
            SingleDevice['R25Calc'] = SingleDevice['Resistance']/(1+0.972*SingleDevice['Power'])
            SingleDevice['R25Error'] = SingleDevice['R25Calc']- LinearFitOffMembrane[i][j][1]
            j=j+1
        i=i+1
    
    return OffMembraneIVData

def AnalyseR25FromError(OffMembraneIVData):
    i=0
    DataSet = pd.DataFrame([],columns=['Power','R25Error'])
    
    for SingleDesign in OffMembraneIVData:
        j=0
        for SingleDevice in SingleDesign:
            OffMembraneIVData[i][j] = OffMembraneIVData[i][j][(OffMembraneIVData[i][j]['Power']>=0.001) & (OffMembraneIVData[i][j]['Power']<=0.005)]
            DataSet = DataSet.append(OffMembraneIVData[i][j])
            j=j+1
        i=i+1
        
    #scatterPlotDataframe2D(OffMembraneIVData,'Power','R25Error','Power (W)','R25 Error (ohms)')
    
    plotForScatter(DataSet,[['Power']],[['R25Error']],[['R25Error']],'Power (W)','R25 Error (ohms)')
    
    i=0;
    DataSet03 = pd.DataFrame([],columns=['Power','R25Error'])
    for SingleDevice in OffMembraneIVData[2]:
            OffMembraneIVData[2][i] = OffMembraneIVData[2][i][(OffMembraneIVData[2][i]['Power']>=0.001) & (OffMembraneIVData[2][i]['Power']<=0.005)]
            DataSet03 = DataSet03.append(OffMembraneIVData[2][i])
            i=i+1
    plotForScatter(DataSet03,[['Power']],[['R25Error']],[['R25Error']],'Power (W)','R25 Error (ohms)')
    
    PrintStats(DataSet03,'R25Error')
    
    

def R25Determination(OffMembraneIVData,OffMembraneLocationData): 
    
    #1. Determine Linear Fit for each device (all data above 0.1V)
    LinearFitOffMembrane1 = getLinearFit(OffMembraneIVData,3)
    
    #2. Plot error in linear fit vs measurement across power level & across voltage
    OffMembraneIVData = determineFitError(OffMembraneIVData,LinearFitOffMembrane1,'FittedResistance','FitError')
    #scatterPlotDataframe2D(OffMembraneIVData,'Power','FitError','Power (W)','Resistance Error (ohms)')
    
    #3. Determine Linear Fit for each device (data above best voltage determined in 2)
    LinearFitOffMembrane2 = getLinearFit(OffMembraneIVData,6)
     
    #4. Plot error in linear fit (from 3) vs measurement across power level & across voltage
    OffMembraneIVData = determineFitError(OffMembraneIVData,LinearFitOffMembrane2,'FittedResistance2','FitError2')
    #scatterPlotDataframe2D(OffMembraneIVData,'Power','FitError2','Power (W)','Resistance Error (ohms)')
    
    #5. Plot difference in the two linear fits (histogram)
    DataFrameLinearFit = get2DArrayToDataFrame(LinearFitOffMembrane1,['LinearCoeff1','OffSet1'])
    DataFrameLinearFit2 = get2DArrayToDataFrame(LinearFitOffMembrane2,['LinearCoeff2','OffSet2'])
    
    DataFrameLinearFit['LinearCoeff2'] = DataFrameLinearFit2['LinearCoeff2']
    DataFrameLinearFit['OffSet2'] = DataFrameLinearFit2['OffSet2']
    DataFrameLinearFit['Difference'] = DataFrameLinearFit['OffSet2'] - DataFrameLinearFit['OffSet1']
    
    #plotHist(DataFrameLinearFit,'Difference','Difference',100)
    
    #6. Determine & Check formula for determining R25 from any power level
    #plotForScatter(DataFrameLinearFit,['OffSet1','OffSet2'],['LinearCoeff1','LinearCoeff2'],['LinearCoeff1','LinearCoeff2'],'R25','LinearCoeff')
    
    OffMembraneIVData = CalculateR25FromMeasurement(OffMembraneIVData,LinearFitOffMembrane1)
    #scatterPlotDataframe2D(OffMembraneIVData,'Power','R25Error','Power (W)','R25 Error (ohms)')
    AnalyseR25FromError(OffMembraneIVData)
    
    OffMembraneIVData = CalculateR25FromMeasurement(OffMembraneIVData,LinearFitOffMembrane2)
    #scatterPlotDataframe2D(OffMembraneIVData,'Power','R25Error','Power (W)','R25 Error (ohms)')
    
    #7. Check With other equipment - for example with a Keithley on packaged parts!

def calculateR25(OffMembraneIVData):
    LinearFitOffMembrane = getLinearFit(OffMembraneIVData,3)
    
    R25 = []
    
    for SingleDesign in LinearFitOffMembrane:
        R25SingleDesign = []
        for SingleDevice in SingleDesign:
            R25SingleDesign.append(SingleDevice[1])
        R25.append(R25SingleDesign)
    
    return R25