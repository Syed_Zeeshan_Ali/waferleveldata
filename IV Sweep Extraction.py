#import necessary imports for data manipulation
import os
import pandas
from matplotlib import pyplot as plt
import numpy as np
from BasicCalcs import *
from ReadFile import *

#Set up constants
alpha=1.95E-3 #Temperature Coefficient Of Resistance
beta=4.50E-7 #Another TCR
TO=25 #Ambient Temperature
R_Track=0 #Resistance of the Tracks (working assumption)

count=0 #The count is just so you can get an idea of when the program will stop running i.e. give you the graph

#Code to read through the files in the folder
os.chdir("C:\\Users\\Zeeshan\\Desktop\\Wafer Level Data\\Raw Power Data\\25C") #Insert directory to folder of files
list1=sorted(filter(os.path.isfile, os.listdir('C:')), key=os.path.getmtime) #Reads through the files in the folder by order of date

#Setting up lists that will be used later to group data
grouppower=[] #For power measurements
grouptemp=[] #For temperature measurements
count = 0;
subdie_types = [1, 2, 3, 4, 5, 6, 8, 9, 10, 35, 36, 37]

DevNo = 3



#Directing through the ListTotal to do things in a specific order, and with each individual file
#for List in ListTotal:
for subList in list1:
    #Using the pandas read function for the csv files (practically excel files) to only read necessary data and to read it in a way such that a dataframe is created
    try:
        StartNum = getStartingLineNumberForFile(subList,"DataName")
        data=pandas.read_csv(subList,skiprows=StartNum, usecols=[1,2]) #Anything up unti row 248 and columns 1&2 is irrelevant
        #(The header number and column numbers can be changed if necessary; this only works if all files are of same format)
    except:
        #list1.remove(subList) #Removing non-CSV files, like text files
        continue
    
    FileNameSplit = sublist.split("_")
    
    ReticleX = int(FileNameSplit[2].split(" ")[0])
    ReticleY = int(FileNameSplit[2].split(" ")[1])
    SubDevID = int(FileNameSplit[3].split(".")[0])%12
    
    DevNo = subdie_types[SubDevID]
    
    
    #Setting up the data columns
    #Firstly, voltage and current for columns 1&2 from the CSV file respectively
    data.columns = ['Current','Voltage'] 
    
    
    #The initial resistance (i.e. the resistance from the first voltage and current values in the dataframe) is collected for future calculations, and also for if you want to compare R0 values of the groups
    R0data=data.loc[0]
    R0=R0data['Voltage']/R0data['Current']
    
    #Power column for the comparisons later
    data['Power']=data['Voltage']*data['Current']*1000
    
    #Resistance column for temperature equation
    data['Resistance']=data['Voltage']/data['Current']
    
    #Setting up quadratic equation values for the temperature equation, as well as working out resistance
    a=beta
    b=alpha-(2*beta*R_Track)
    data['c'] = -(data['Resistance']-R_Track)/(R0-R_Track)+(1-alpha*TO+beta*(TO**2))
    
    #Working out temperatures from the provided equation, where value errors are turned into nans
    try:
        data['Temperature'] = ((-b+(((b**2)-4*a*data['c']))**(1/2))/(2*a))-24.71481034
    except ValueError:
        data['Temperature']=np.nan
    
    #More data was erroneous because the device never really heated up. If the sum of the temperatures for the dataframe was less than 1000, it was taken as improperly measured and so had to be ignored
    tempsum=sum(data['Temperature'])
    if tempsum<=1000:
        continue

    else:
        
        #Numpy arrays added to separate lists for transportation purposes
        grouppower.append(data['Power'].to_numpy())
        grouptemp.append(data['Temperature'].to_numpy())
        
    count = count + 1
    print(str(count))
#Sublists grouped by means of adding list to another list. First list's values were then erased so the list became usable again
#power=np.append(power,grouppower)
#grouppower.clear()
#temperature=np.append(temperature,grouppower)
#grouptemp.clear()

#Count, for timing purposes. Not necessary to code
#count=count+1
#print (count)
    
#Reshaping of arrays so the groups could be meaned    
#powerreshape=power.reshape(1,158,20)
#temperaturereshape=temperature.reshape(1,158,20)
powerreshape=grouppower
temperaturereshape = grouptemp

#Meaning of groups, ignoring nan values to get numpy array rows for each design
#powermean=np.nanmean(powerreshape)
#temperaturemean=np.nanmean(temperaturereshape)

#Plotting the mean values against one another
plot1=plt.plot(grouptemp[0],grouppower[0],linewidth=1)
for x in range(len(grouptemp)-1):
    plt.plot(grouptemp[x+1],grouppower[x+1],linewidth=1)

#Labelling the axes
plt.ylabel("Power (mW)",fontsize=30)
plt.xlabel("Change in Temperature (˚C)", fontsize=30)

#Annotating the lines
#plt.legend(plot,['Chip 1','Chip 2','Chip 3','Chip 4','Chip 5','Chip 6','Chip 8','Chip 9','Chip 10','Chip 11','Chip 12','Chip 13','Chip 14','Chip 15','Chip 17','Chip 18','Chip 19','Chip 20','Chip 21','Chip 22','Chip 25','Chip 28','Chip 29','Chip 30','Chip 31','Chip 35','Chip 36','Chip 37','Chip 39','Chip 40','Chip 41','Chip 42','Chip 43','Chip 44'], prop={"size":12})

#Deciding the increments of the axes
#plt.yticks([0,2,4,6,8,10,12,14,16,18,20,22,24,26],fontsize=20)
#plt.xticks([0,20,40,60,80,100,120,140,160,180,200,220,240,260,280,300,320,340,360,380,400,420,440,460,480,500],fontsize=20)

#Showing the graph
plt.show()