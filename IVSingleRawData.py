# -*- coding: utf-8 -*-
"""
Created on Mon Aug 24 12:56:21 2020

@author: Zeeshan
"""

import pandas as pd
import numpy as np
from BasicCalcs import *
import glob

class IVSingleRawData:
    
    IVData = pd.DataFrame()
    LocationX = 0
    LocationY = 0
    ItemNo = 0
    Temperature = 25
    ResistanceTMeas = 120
    ResistanceT25 = 120
    OnMembrane = False
    Power230 = 14.00
    TCR2 = 0.4e-6
    
    
    def __init__(self,inputFileName,inputTemp,inputOnMem):
        self.OnMembrane = inputOnMem
        self.Temperature = inputTemp
        self.IVData = self.getDataFromFile(inputFileName)
        (self.LocationX,self.LocationY,self.ItemNo) = self.getDeviceLocation(inputFileName)
        
        self.IVData = self.CalculateFileData(self.IVData)
    
    
#############################################################################################################################
# Getting Data From File
    
    def getDataFromFile(self,FilePathAndName):
        StartNum = self.getStartingLineNumberForFile(FilePathAndName,"DataName")
        FileData=pd.read_csv(FilePathAndName,skiprows=StartNum, usecols=[1,2])
        FileData.columns = ['Current','Voltage'] 
        return FileData
    
    def getStartingLineNumberForFile(self,FileName,lookup):
        with open(FileName) as myFile:
            for num, line in enumerate(myFile, 1):
                if lookup in line:
                    return num
            myFile.close();
        return 0
    
    def getDeviceLocation(self,FilePathAndName):
        FileNameSplit = FilePathAndName.split("_")
        
        SplitLen = len(FileNameSplit)
        
        ReticleX = int(FileNameSplit[SplitLen-2].split(" ")[1])
        ReticleY = int(FileNameSplit[SplitLen-2].split(" ")[2])
        DevNo = int(FileNameSplit[SplitLen-1].split(".")[0])
        return (ReticleX,ReticleY,DevNo)


##################################################################################################################################
# Calculations on Data

    def CalculateFileData(self,SingleDataSet):
        SingleDataSet['Resistance'] = SingleDataSet['Voltage']/SingleDataSet['Current']
        SingleDataSet['Power'] = SingleDataSet['Voltage']*SingleDataSet['Current']
        R25 = SingleDataSet['Resistance'][0]
        try:
            QuadFit = np.polyfit(self.IVData['Power'].to_numpy()[3:], self.IVData['Resistance'].to_numpy()[3:], 2)
            self.ResistanceTMeas = QuadFit[2]
            R25 = getR25fromResistance(self.ResistanceTMeas,self.Temperature)
            self.ResistanceT25 = R25
        except:
            self.ResistanceTMeas = R25      
            self.ResistanceT25 = R25 = getR25fromResistance(self.ResistanceTMeas,self.Temperature)
            self.Power230 = np.nan
        
        if self.ResistanceTMeas == np.nan:
            TCR1 = getTCR1(R25)
            SingleDataSet['Temperature'] = getTemperatureFromResistance(SingleDataSet['Resistance'], R25, TCR1,self.TCR2,25)
        else:
            TCR1 = getTCR1(self.ResistanceT25)
            SingleDataSet['Temperature'] = getTemperatureFromResistance(SingleDataSet['Resistance'], self.ResistanceT25, TCR1,self.TCR2,25)
        
        if self.OnMembrane == True:
            self.getPower230()
        
        return SingleDataSet
    
    def checkResistance(self):
        if (ResistanceTMeas==np.nan) | (self.ResistanceT25==np.nan):
            return False
        
        if (self.ResistanceT25 <150) & (self.ResistanceT25>80):
            return True
        
        return False
    
    def checkPower(self):
        if self.Power230 == np.nan:
            return False
                
        if self.Power230 > 50:
            return False
        
        return True
    
    def checkData(self):
        if (self.checkResistance()==True) & (self.checkPower()==True):
            return True
        return False
        
    
    
    def getPower230(self):
        a=0
        if self.OnMembrane==False:
            self.Power230=np.nan
            return
        if self.ResistanceTMeas==np.nan:
            self.Power230=np.nan
            return
        if (self.ResistanceT25>150) | (self.ResistanceT25<80):
            self.Power230=np.nan
            return
        
        try:
            QuadFit = np.polyfit(self.IVData['Temperature'].to_numpy()[3:], self.IVData['Power'].to_numpy()[3:], 2)
            self.Power230 = QuadFit[0]*230*230 + QuadFit[1]*230+QuadFit[2]
        except:
            a=1
            self.Power230=np.nan
        