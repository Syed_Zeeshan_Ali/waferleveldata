# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 15:25:23 2020

@author: Zeeshan
"""

from tkinter import filedialog

def getFolderList(TempList):
    FolderList = []
    
    for SingleTemp in TempList:
        WindowTitle = 'Directory of ' + str(SingleTemp) + 'C Files'
        SingleFolderPath = filedialog.askdirectory(title=WindowTitle)
        FolderList.append(SingleFolderPath)
    
    return FolderList