import Globals
from Globals import *

#def getTCR1(R0Resistance):
#    return -0.004478*R0Resistance/340 + 0.0037765

def getTCR1(R0Resistance):
    return 0.0022 - 0.0015*(R0Resistance-118)/340

#def getTCR1(R0Resistance):
#    return 0.0022

def getR25fromResistance(Resistance,Temp):
    if Temp==25:
        return Resistance
    
    U=Temp-25
    V=3.7765e-3
    M = -4.478e-3
    N = 340
    
    return ((-1)*(1+U*V+U*U*TCR2)+((1+U*V+U*U*TCR2)**2+(4*U*M*Resistance)/(N))**0.5)/(2*U*M/N)

def getTemperatureFromResistance(Resistance, R0, TC1,TC2,Tamb):    
    return ((-1)*TC1+(TC1*TC1 - 4*TC2*(1-Resistance/R0))**(1/2))/(2*TC2) + Tamb 

def calculateResistance(R25,Temp,TC1,TC2):
    return R25*(1 + TC1*(Temp-25) + TC2*(Temp-25)*(Temp-25))
    
def calculatePnoflowNewLinear(DieTemperature):
 #   return 15.279-0.05775*DieTemperature 
    return 15.15-0.05152*DieTemperature - 0.00005498*DieTemperature*DieTemperature
    
def calculatehCoef(Power, Pnoflow, DieTemperature):
    return (Power - Pnoflow)/(OperatingTemperature-DieTemperature)

def getPNoFlow(DieTemperature,CoefA,CoefB,CoefC):
    return CoefC - DieTemperature*CoefB - DieTemperature*DieTemperature*CoefA

def calculatehCo(Power,PNoFlow,SetTemp,AmbientTemp):
    return (Power-PNoFlow)/(SetTemp - AmbientTemp)

def getR25(Measured_Resistance,Measured_Power):
    return Measured_Resistance/(1+Measured_Power*0.972)


def PrintStats(MainDataSet,Column):
    Mean = MainDataSet[Column].mean()
    Std = MainDataSet[Column].std()
    Min = MainDataSet[Column].min()
    Max = MainDataSet[Column].max()
    
    print(Column + ", Mean: " + str(Mean) + ", Std: " + str(Std) + ", Min: " + str(Min) + ", Max: " + str(Max))
