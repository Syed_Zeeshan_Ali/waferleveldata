import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
import Globals
from Globals import *
from BasicCalcs import *
from ReadFile import *
from DataPlots import *
from ResistanceCompare import *
from TCRAnalysis import *
from R25Determination import *
from PowerAnalysis import *
#from ExtraPlots import *
#from PiccoloPlot import *
#from CurveFitsAndPlots import *
from tkinter import filedialog
from scipy.optimize import curve_fit



ChipNumbers = [1]

#Get path and names of Electrical and flow files
FolderPath = filedialog.askdirectory(title='Directory of 25C Files')
FolderPath75 = filedialog.askdirectory(title='Directory of 75C Files')
FolderPath125 = filedialog.askdirectory(title='Directory of 125C Files')



OnMembraneIVData,OffMembraneIVData,OnMembraneLocationData,OffMembraneLocationData = CollectDataFromFolder(FolderPath,ChipNumbers)
OnMembraneIVData75,OffMembraneIVData75,OnMembraneLocationData75,OffMembraneLocationData75 = CollectDataFromFolder(FolderPath75,ChipNumbers)
OnMembraneIVData125,OffMembraneIVData125,OnMembraneLocationData125,OffMembraneLocationData125 = CollectDataFromFolder(FolderPath125,ChipNumbers)

AnalysePower(OffMembraneIVData,OnMembraneIVData,OffMembraneIVData75,OnMembraneIVData75,OffMembraneIVData125,OnMembraneIVData125,OnMembraneLocationData)



#Tasks:
#1. Accurate Determination of R25 of Temp sensor
#R25Determination(OffMembraneIVData,OffMembraneLocationData)





#2. Accurate Determination of R25 of Heater
    #(Available from summarized data)
    
#3. Accurate determination of TCR & Heater Setpoint resistance
    #(Available from Summarized Data)
        
#4. Accurate determination of Power Requirements
    #1. Calculate R25 for all Chip 03 designs
    #2. For each chip 3 device and data point calculate heater temperature
    #3. Plot power for each chip 3 device vs heater temperature
    #4. Plot power for each chip 3 device vs heater temperature at different ambient temperatures
    #5. Extract Power coefficients for #3. Calculate power at 200C and plot Coefficients against P200



#DataChip3 = ResistanceComparisonRaw(OnMembraneIVData[3],OffMembraneIVData[3])