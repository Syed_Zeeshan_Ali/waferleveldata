"""
This script is to rename the files that are run on a separate test after the wafer
prober decides to stop in the middle of the night!
@author: Ethan
"""

import glob
import os
import re

def natural_sort(list, key=lambda s:s):
    """
    Sort the list into natural alphanumeric order.
    """
    def get_alphanum_key_func(key):
        convert = lambda text: int(text) if text.isdigit() else text
        return lambda s: [convert(c) for c in re.split('([0-9]+)', key(s))]
    sort_key = get_alphanum_key_func(key)
    list.sort(key=sort_key)

filelist = [os.path.normpath(i) for i in sorted(glob.glob("E:/H23480-13 (2nd Measurements)/125 degC/H23480-13/125CREST/*.csv"))]
natural_sort(filelist, key=lambda x: x.split('_')[3].split('.')[0])

# Define here the test numer that it should be on the full wafer measurement minus 1
device_number = 4498

for oldname in filelist:
    # ignore directories
    if os.path.isfile(oldname):
        # keep original path
        testnumber = oldname.split('_')[3].split('.')[0]
        basepath = os.path.split(oldname)[0]
        newbasename = '_'.join(oldname.split('_')[:-1])
        newtestnumber = int(testnumber) + device_number
        newname = os.path.join(basepath, f"{newbasename}_{newtestnumber}.csv")

        print("Renaming {} to {}".format(oldname, newname))
        os.rename(oldname, newname)
